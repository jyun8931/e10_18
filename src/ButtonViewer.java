import javax.swing.*;
import java.awt.event.ActionListener;

public class ButtonViewer {
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 110;

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        JButton button1 = new JButton("Button 1");
        JButton button2 = new JButton("Button 2");
        panel.add(button1);
        panel.add(button2);

        ActionListener listener1 = new ClickListener();
        ActionListener listener2 = new ClickListener();
        button1.addActionListener(listener1);
        button2.addActionListener(listener2);
        frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
