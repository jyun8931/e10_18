import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClickListener implements ActionListener {
    private int numClicks;
    public void actionPerformed(ActionEvent event){
        numClicks++;
        System.out.printf("I was clicked %d times\n", numClicks);

    }
}
